from model import Car

class Writer():
    def __init__(self, file_name):
        self.file_name = file_name

    def write(self, cars):
        with open(self.file_name, "w") as f:
            n = len(cars) - 1
            i = 0
            for x in cars:
                i += 1
                f.write(x.to_string())
                if i <= n:
                    f.write("\n")
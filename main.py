from reader import Reader
from writer import Writer
from eugen import Alg

# letter = "a"
# letter = "b"
# letter = "c"
# letter = "d"
# letter = "e"

letter = "c"

input_file_name = "input/{0}.in".format(letter)
output_file_name = "output/{0}.out".format(letter)

def __main__():
    reader = Reader(input_file_name)
    h, rides = reader.read()
    
    alg = Alg()
    
    cars = alg.run(h, rides)

    writer = Writer(output_file_name)
    writer.write(cars)

__main__()

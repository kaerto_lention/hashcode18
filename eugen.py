from model import Car, Header, Ride

class Alg():

    def run(self, header, rides):
        
        cars = [Car() for i in range(header.vehicles_nr)]
        
        # new_rides = list(filter(lambda x:  in_city(x, header) , rides))
        new_rides = rides

        # new_rides.sort(key =  lambda x: x.time_start)
        # new_rides.sort(key =  lambda x: ((header.rows/2 - x.end_row)**2 + (header.columns/2 - x.end_col)**2)**0.5, reverse=True )
        new_rides.sort(key =  lambda x: ride_len(x), reverse=True)

        # print(ride_len(new_rides[0]))
        # print(ride_len(new_rides[100]))

        divider = 100
        n = int(len(new_rides) / divider)
        
        i = 0
        for x in range(n):
            r = new_rides[n*x: n*(x+1)]
            i = add_rides(cars, r, i)
    
        
        return cars


def in_city(x, h):
    return x.start_row < h.rows/2 and x.start_col < h.columns/2 and x.end_row < h.rows/2 and x.end_col < h.columns/2

def ride_len(x):
    return abs(x.end_row-x.start_row) + abs(x.end_col - x.start_col)

def add_rides(cars, rides, index):
    i1 = 0
    i2 = index
    n = len(rides) - 1
    c_n = len(cars) - 1
    
    while i1 < n:
        if i2 > c_n: 
            i2 = 0
        
        cars[i2].rides.append(rides[0])
        rides.remove(rides[0])
        
        i1 += 1
        i2 += 1

    return i2
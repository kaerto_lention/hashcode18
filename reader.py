from model import Header, Ride

class Reader():
    def __init__(self, file_name):
        self.file_name = file_name

    def read(self):

        header = Header()
        rides = []

        with open(self.file_name, "r") as f:
            header.read(list(map(int, f.readline().split(" "))))
            for i in range(header.rides):
                t = list(map(int, f.readline().split(" ")))
                x = Ride()
                x.read(t, i)
                rides.append(x)
        return (header, rides)

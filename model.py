class Header():
    def read(self, t):
        self.rows = t[0]
        self.columns = t[1]
        self.vehicles_nr = t[2]
        self.rides = t[3]
        self.on_time_bonus = t[4]
        self.simulation_steps = t[5]

class Ride():
    def read(self, t, id):
        self.start_row = t[0]
        self.start_col = t[1]
        self.end_row = t[2]
        self.end_col = t[3]
        self.time_start = t[4]
        self.time_end = t[5]
        self.id = id

class Car():
    def __init__(self):
        self.rides = []

    def to_string(self):
        s = ""
        s += "{0}".format(len(self.rides))
        for x in self.rides:
            s += " {0}".format(x.id) 
        return s